// pages/api/screenshot.ts
import { NextApiRequest, NextApiResponse } from 'next';
import puppeteer from 'puppeteer-core';
const chromium = require("@sparticuz/chromium");
import { LRUCache } from 'lru-cache'

const screenshotCache = new LRUCache<string, Buffer>({ max: 100 }); 

let browserInstance: any;

async function launchBrowser() {
	if (!browserInstance) {
		const isProd = process.env.NODE_ENV === 'production'
		if (isProd) {
			browserInstance = await puppeteer.launch({
				args: chromium.args,
				defaultViewport: chromium.defaultViewport,
				executablePath: await chromium.executablePath(),
				headless: chromium.headless,
				});
		} else {
			browserInstance = await puppeteer.launch({
				headless: 'new',
				executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
			})
		}
	}
}
	

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	if (req.method !== 'GET') {
		return res.status(405).json({ error: 'Method Not Allowed' });
	}
	
	const { url, delay } = req.query;
	
	if (!url) {
		return res.status(400).json({ error: 'Missing URL parameter' });
	}
	const cacheKey = encodeURIComponent(url as string);
	
	// Check if the screenshot is in the cache
	const cachedScreenshot = screenshotCache.get(cacheKey);
	if (cachedScreenshot) {
		// Serve the cached screenshot
		res.setHeader('Content-Type', 'image/png');
		res.end(cachedScreenshot);
		return;
	}
	let page;
	try {
		await launchBrowser();
		page = await browserInstance.newPage();
		const userAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1';
		await page.setUserAgent(userAgent);
		page.setViewport({
			width: 540,
			height: 960,
		});
		await page.goto(url as string); // Casting since URL should be a string
		if (delay) {
			await new Promise(f => setTimeout(f, parseInt(delay as string) * 1000));
		}
		// Capture screenshot as a Buffer
		const screenshot = await page.screenshot();
	
		await page.close();
	
		screenshotCache.set(cacheKey, screenshot);
	
		// Set the appropriate headers for an image
		res.setHeader('Content-Type', 'image/png');
	
		// Send the image buffer as the response
		res.end(screenshot);
	} catch (error) {
		if (page)
		{
			page.close();
		}
		console.error('Error capturing screenshot:', error);
		res.status(500).json({ error: 'Internal Server Error' });
	}
}